import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'person-detail-component',
  templateUrl: './base-widget.component.html',
  styleUrls: ['./base-widget.component.css']
})
export class BaseWidgetComponent implements OnInit {

  public dataPerson: Array<any> = [{"displayname": "Donald Trump","image": "../assets/noimg.png","counter_news": "18,264 news","counter_statement": "8,432",	"related_media": "News Yahoo Singapore (1654), The Times of India (1251), Channel News Asia (994) and more"}];

  constructor() {console.log(this.dataPerson);}
  
  ngOnInit() { 
  }

}
 